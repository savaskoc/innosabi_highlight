const {search, highlight, highlighter} = require('../main')

describe('main.js', () => {
    const highlighted = text => `<strong class="highlight">${text}</strong>`
    let template

    beforeEach(() => {
        document.body.innerHTML = `
            <form class="form-inline">
                <label class="sr-only" for="search">Search</label>
                <div class="input-group">
                    <input id="search" type="text" class="form-control" placeholder="Highlight in text">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary">Search</button>
                    </span>
                </div>
            </form>
            <div class="search-content">
                <h2>Perceived end knowledge certainly day sweetness why cordially</h2>
                <p>Unpacked reserved sir offering <strong>bed judgment may and quitting speaking</strong>. Is do be
                    improved raptures offering required in replying raillery. Stairs ladies friend by in mutual an no.
                    Mr hence chief he cause. Whole no doors on hoped. Mile tell if help they ye full name.</p>
                <p>Behaviour we improving at something to. Evil true high lady roof men had open. To
                    <b class="bold">projection</b> considered it precaution an melancholy or. Wound young you thing
                    worse along being ham. Dissimilar of favourable solicitude if sympathize middletons at. Forfeited
                    up if disposing perfectly in an eagerness perceived necessary. Belonging sir curiosity discovery
                    extremity yet forfeited prevailed own off. Travelling by introduced of mr terminated. Knew as miss
                    my high hope quit. In curiosity shameless dependent knowledge up.</p>
                <p>&lt;strong&gt;Test&lt;/strong&gt;</p>
                <p>regexTest()[]</p>
            </div>
        `
        template = document.querySelector('.search-content')
    })

    describe('search', () => {
        const searchWrapper = query => search(template, query).innerHTML
        const searched = text => `[h(${text})]`

        it('should find the text', () => {
            expect(searchWrapper('bed')).toMatch(searched('bed'))
        })

        it('should find multiple texts', () => {
            const html = searchWrapper('bed eser')
            expect(html).toMatch(searched('bed'))
            expect(html).toMatch(searched('eser'))
        })

        it('should find special characters', () => {
            expect(searchWrapper(')[')).toMatch(searched(')['))
        })

        it('should do case-insensitive search', () => {
            expect(searchWrapper('BeD')).toMatch(searched('bed'))
        })

        it('should not modify html tags', () => {
            const html = searchWrapper('projection est')
            expect(html).toMatch(`<b class="bold">${searched('projection')}</b>`)
            expect(html).toMatch(`&lt;strong&gt;T${searched('est')}&lt;/strong&gt;`)
        })

        it('should not modify document', () => {
            searchWrapper('bed')
            expect(document.body.innerHTML).not.toMatch(searched('bed'))
        })
    })

    describe('highlight', () => {
        const highlightWrapper = query => highlight(template, query)

        it('should highlight the text', () => {
            expect(highlightWrapper('bed')).toMatch(highlighted('bed'))
        })

        it('should highlight multiple texts', () => {
            const html = highlightWrapper('bed eser')
            expect(html).toMatch(highlighted('bed'))
            expect(html).toMatch(highlighted('eser'))
        })

        it('should highlight special characters', () => {
            expect(highlightWrapper(')[')).toMatch(highlighted(')['))
        })

        it('should do case-insensitive highlight', () => {
            expect(highlightWrapper('BeD')).toMatch(highlighted('bed'))
        })

        it('should not modify html tags', () => {
            const html = highlightWrapper('projection est')
            expect(html).toMatch(`<b class="bold">${highlighted('projection')}</b>`)
            expect(html).toMatch(`&lt;strong&gt;T${highlighted('est')}&lt;/strong&gt;`)
        })

        it('should not modify document', () => {
            highlightWrapper('bed')
            expect(document.body.innerHTML).not.toMatch(highlighted('bed'))
        })
    })

    describe('highlighter', () => {
        const submitForm = (input, text) => {
            input.value = text
            getForm(input).dispatchEvent(new Event('submit'))
        }

        const getContainer = () => document.querySelector('.search-content')
        const getInput = () => document.getElementById('search')
        const getForm = input => input.closest('form')

        beforeEach(() => {
            highlighter(getContainer(), getInput())
        })

        it('should highlight the text', () => {
            submitForm(getInput(), 'bed')
            expect(document.body.innerHTML).toMatch(highlighted('bed'))
        })

        it('should highlight multiple texts', () => {
            submitForm(getInput(), 'bed eser')
            expect(document.body.innerHTML).toMatch(highlighted('bed'))
            expect(document.body.innerHTML).toMatch(highlighted('eser'))
        })

        it('should highlight special characters', () => {
            submitForm(getInput(), ')[')
            expect(document.body.innerHTML).toMatch(highlighted(')['))
        })

        it('should do case-insensitive highlight', () => {
            submitForm(getInput(), 'BeD')
            expect(document.body.innerHTML).toMatch(highlighted('bed'))
        })

        it('should not modify html tags', () => {
            submitForm(getInput(), 'projection est')
            expect(document.body.innerHTML).toMatch(`<b class="bold">${highlighted('projection')}</b>`)
            expect(document.body.innerHTML).toMatch(`&lt;strong&gt;T${highlighted('est')}&lt;/strong&gt;`)
        })

        it('should not persist the results after highlight', () => {
            submitForm(getInput(), 'projection')
            submitForm(getInput(), 'est')
            expect(document.body.innerHTML).toMatch(highlighted('est'))
            expect(document.body.innerHTML).not.toMatch(highlighted('projection'))
        })
    })
})
