(function () {
    //Taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
    const escapeRegExp = string => string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&')
    const nodeFilter = {
        acceptNode: function (node) {
            if (!/^\s*$/.test(node.data)) {
                return NodeFilter.FILTER_ACCEPT
            }
        }
    }

    function search(template, query) {
        const root = template.cloneNode(true)
        const walker = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, nodeFilter)
        const regexp = new RegExp(query.split(/\s+/).map(escapeRegExp).join('|'), 'gi')

        let current
        while (current = walker.nextNode()) {
            current.textContent = current.textContent.replace(regexp, '[h($&)]')
        }

        return root
    }

    function highlight(template, query) {
        return search(template, query).innerHTML.replace(/\[h\((.*?)\)]/g, '<strong class="highlight">$1</strong>')
    }

    function highlighter(container, input) {
        const template = container.cloneNode(true)
        input.closest('form').addEventListener('submit', function (e) {
            e.preventDefault()
            container.innerHTML = highlight(template, input.value)
        })
    }

    if (typeof exports === "object") {
        module.exports = {search, highlight, highlighter}
        return
    }
    highlighter(document.querySelector('.search-content'), document.getElementById('search'))
})()
